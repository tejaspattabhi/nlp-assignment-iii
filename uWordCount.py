import json
import copy
import re

from collections import Counter

tSentences = re.findall (".*", open ('training.txt').read())
try:
	tSentences = filter (lambda a: a != "", tSentences)
	tSentences = filter (lambda a: a != "\t", tSentences)
except:
	"Do Nothing!"

# print tSentences
tWords = []
for s in tSentences:
	# print s
	ps = s.split (' ')
	# print ps
	for pair in ps:
		word = pair.split ('/')[0]
		# print not (word in tWords)
		tWords.append (word)
tWords.extend (['<s>', '<es>'])

tWords = list (set (tWords))

with open ("uWords.json", "w") as file:
	json.dump (tWords, file, indent = 2, sort_keys = True)
	file.close ()